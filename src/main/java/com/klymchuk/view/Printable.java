package com.klymchuk.view;

public interface Printable {
    void print();
}
