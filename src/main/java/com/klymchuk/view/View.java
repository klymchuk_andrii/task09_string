package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import com.klymchuk.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class View implements Printable {
    private Controller controller;

    private Logger logger;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private Locale locale;
    private ResourceBundle bundle;


    public View() {
        controller = new ControllerImp();
        logger = LogManager.getLogger(View.class);

        input = new Scanner(System.in);

        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);

        setMenu();

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::concatenateAllParameters);
        methodsMenu.put("2", this::checkTextForPattern);
        methodsMenu.put("3", this::splitSentenceByTheAndYou);
        methodsMenu.put("4", this::changeAllVowels);
        methodsMenu.put("5", this::readFromFile);
        methodsMenu.put("6",this::printSentencesByWordCount);
        methodsMenu.put("7",this::checkUniqueWorld);
        methodsMenu.put("8",this::changeWordsWithFirstVowelsLetter);
        methodsMenu.put("9",this::printAllWorlds);
        methodsMenu.put("10",this::printWorldsSortedByPercentVowelLetters);
        methodsMenu.put("11",this::printWorldsSortedByFirstConsonantLetter);

        controller.printSentences();
    }

    public void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("10", bundle.getString("10"));
        menu.put("11", bundle.getString("11"));
        menu.put("Q", bundle.getString("Q"));
    }

    private void Menu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            Menu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    public void concatenateAllParameters() {
        logger.trace(controller.concatenatesAllParameters(input.nextLine(), input.nextLine()));
    }

    public void checkTextForPattern() {
        logger.trace("text " + controller.checkTextForPattern(input.nextLine()));
    }

    public void splitSentenceByTheAndYou() {
        logger.trace(controller.splitSentenceByTheAndYou(input.nextLine()));
    }

    public void changeAllVowels() {
        logger.trace(controller.changeAllVowels(input.nextLine()));
    }

    public void readFromFile() {
        logger.trace(controller.getFromFile());
    }

    public void printSentencesByWordCount() {
        logger.trace(controller.printSentencesByWordCount());
    }

    public void checkUniqueWorld(){
        logger.trace(controller.checkUniqueWorld());
    }

    public void changeWordsWithFirstVowelsLetter(){
        logger.trace(controller.changeWordsWithFirstVowelsLetter());
    }

    public void printAllWorlds(){
        logger.trace(controller.printAllWords());
    }

    public void printWorldsSortedByPercentVowelLetters(){
        logger.trace(controller.printWorldsSortedByPercentVowelLetters());
    }

    public void printWorldsSortedByFirstConsonantLetter(){
        logger.trace(controller.printWorldsSortedByFirstConsonantLetter());
    }

    @Override
    public void print() {

    }

}