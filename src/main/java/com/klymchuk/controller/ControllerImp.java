package com.klymchuk.controller;

import com.klymchuk.model.BusinessLogic;
import com.klymchuk.model.Model;

public class ControllerImp implements Controller {
    private Model model;

    public ControllerImp() {
        model = new BusinessLogic();
    }

    public String concatenatesAllParameters(Object... objects) {
        return model.concatenatesAllParameters(objects);
    }

    public String checkTextForPattern(String text) {
        return model.checkTextForPattern(text) ? "coincides" : "is different";
    }

    public String splitSentenceByTheAndYou(String text) {
        return model.splitSentenceByTheAndYou(text);
    }

    public String changeAllVowels(String text) {
        return model.changeAllVowels(text);
    }

    public void printSentences() {
        model.printSentences();
    }

    public String getFromFile() {
        return model.getFromFile();
    }

    public String printSentencesByWordCount() {
        return model.printSentencesByWordCount();
    }

    public String checkUniqueWorld() {
        return model.checkUniqueWorld();
    }

    public String changeWordsWithFirstVowelsLetter() {
        return model.changeWordsWithFirstVowelsLetter();
    }
    public String printAllWords(){
        return model.printAllWords();
    }

    public String printWorldsSortedByPercentVowelLetters(){
        return model.printWorldsSortedByPercentVowelLetters();
    }
    public String printWorldsSortedByFirstConsonantLetter(){
        return model.printWorldsSortedByFirstConsonantLetter();
    }
}
