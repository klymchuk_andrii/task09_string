package com.klymchuk.controller;

public interface Controller {
    String concatenatesAllParameters(Object... objects);

    String checkTextForPattern(String text);

    String splitSentenceByTheAndYou(String text);

    String changeAllVowels(String text);

    void printSentences();

    String getFromFile();

    String printSentencesByWordCount();

    String checkUniqueWorld();

    String changeWordsWithFirstVowelsLetter();

    String printAllWords();

    String printWorldsSortedByPercentVowelLetters();

    String printWorldsSortedByFirstConsonantLetter();
}
