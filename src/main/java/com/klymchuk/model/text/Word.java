package com.klymchuk.model.text;

public class Word {
    private String word;

    public Word(String wordParam) {
        word = wordParam;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object obj) {
        Word other = (Word) obj;
        return word.equals(other.getWord());
    }

    @Override
    public String toString() {
        return word + " ";
    }
}
