package com.klymchuk.model.text;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
    private String sentence;
    private List<Word> wordList;

    public Sentence(String sentenceParam) {
        sentence = sentenceParam;
        wordList = new ArrayList<>();
        initializationList(sentence);
    }

    public void initializationList(String sentenceParam) {
        String[] strings = sentenceParam.split("\\s|,|:|-");
        for (String s : strings) {
            if(!s.equals("")) {
                wordList.add(new Word(s));
            }
        }
    }

    public String getSentence() {
        return sentence;
    }

    public List<Word> getWordList() {
        return wordList;
    }

    public boolean checkOfEquals(Word word,int number) {
        int count = 0;
        for (Word w : wordList) {
            if (w.equals(word)) {
                count++;
            }
        }
        return count > number-1;
    }



    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for(Word w:wordList){
            stringBuilder.append(w);
        }
        return stringBuilder.toString();
    }
}
