package com.klymchuk.model;

import com.klymchuk.model.text.Sentence;
import com.klymchuk.model.text.Word;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class BusinessLogic implements Model {
    private StringUtils stringUtils;
    private List<Sentence> sentenceList;

    public BusinessLogic() {
        stringUtils = new StringUtils();
        sentenceList = new ArrayList<>();
    }

    public String concatenatesAllParameters(Object... objects) {
        return stringUtils.concatenateAllParameters(objects);
    }

    public boolean checkTextForPattern(String text) {
        return stringUtils.checkTextForPattern(text);
    }

    public String splitSentenceByTheAndYou(String text) {
        return stringUtils.splitSentenceByTheAndYou(text);
    }

    public String changeAllVowels(String text) {
        return stringUtils.changeAllVowels(text);
    }

    public String getFromFile() {
        String filePath = "C:\\Users\\Andrii\\IdeaProjects\\task9\\text.txt";

        StringBuilder stringBuilder = new StringBuilder();

        try (FileReader reader = new FileReader(filePath)) {
            int c;
            while ((c = reader.read()) != -1) {
                stringBuilder.append((char) c);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        String text = stringBuilder.toString();

        String[] sentences = text.split("[.]|[?]|!");

        for (String s : sentences) {
            s = s.replaceAll("\n", "");
            s = s.replaceAll("\r|\\s{2,}", " ");
            sentenceList.add(new Sentence(s));
        }

        return "successful read from file";
    }

    public void printSentences() {
        System.out.println(sentenceList);
        System.out.println(countOfSentencesWithEqualWords());
    }

    public int countOfSentencesWithEqualWords() {
        int count = 0;
        for (Sentence s : sentenceList) {
            for (Word w : s.getWordList()) {
                if (s.checkOfEquals(w, 2)) {
                    count++;
                    break;
                }
            }
        }
        return count;
    }

    public String printSentencesByWordCount() {
        List<Sentence> list = sentenceList;
        list.sort(Comparator.comparing(s -> s.getWordList().size()));
        StringBuilder stringBuilder = new StringBuilder();
        for (Sentence s : list) {
            stringBuilder.append(s.getSentence()).append("\n");
        }
        return stringBuilder.toString();
    }

    public String checkUniqueWorld() {
        String answer = "";
        int count = 0;
        for (Word w : sentenceList.get(0).getWordList()) {
            count = 0;
            for (int i = 1; i < sentenceList.size(); i++) {
                if (sentenceList.get(i).checkOfEquals(w, 1)) {
                    break;
                }
                count++;
            }
            if (count == sentenceList.size() - 1) {
                answer += w.getWord() + " ";
            }
        }
        if (answer.equals("")) {
            return "Word don`t exist";
        }
        return answer;
    }

    public String changeWordsWithFirstVowelsLetter() {
        StringBuilder stringBuilder = new StringBuilder();

        Word temp;
        int indexOfMaxLength = 0;

        List<Sentence> list = sentenceList;

        Pattern pattern = Pattern.compile("^[AOEUIieauo]+.*");

        for (Sentence s : list) {
            if (pattern.matcher(s.getWordList().get(0).getWord()).find()) {
                temp = s.getWordList().get(0);
                indexOfMaxLength = getIndexOfBiggerElement(s);
                s.getWordList().set(0, s.getWordList().get(indexOfMaxLength));
                s.getWordList().set(indexOfMaxLength, temp);
            }
        }

        for (Sentence sentence : list) {
            stringBuilder.append(sentence).append("\n");
        }
        return stringBuilder.toString();
    }

    private int getIndexOfBiggerElement(Sentence s) {
        int maxLength = s.getWordList().get(1).getWord().length();
        int index = 0;
        for (int i = 1; i < s.getWordList().size(); i++) {
            if (s.getWordList().get(i).getWord().length()
                    > maxLength) {
                maxLength = s.getWordList().get(i).getWord().length();
                index = i;
            }
        }
        return index;
    }

    public String printAllWords() {
        List<Word> list = new ArrayList<>();

        StringBuilder stringBuilder = new StringBuilder();

        for (Sentence s : sentenceList) {
            for (Word word : s.getWordList()) {
                if (list.indexOf(word) == -1) {
                    list.add(word);
                }
            }
        }

        for (Word w : list) {
            w.setWord(w.getWord().toLowerCase());
        }

        list.sort(Comparator.comparing(Word::getWord));

        stringBuilder.append("  ").append(list.get(0)).append("\n");
        for (int i = 1; i < list.size() - 1; i++) {
            if (list.get(i).getWord().charAt(0) != list.get(i - 1).getWord().charAt(0)) {
                stringBuilder.append("   ").append(list.get(i)).append("\n");
            } else {
                stringBuilder.append(list.get(i)).append("\n");
            }
        }
        return stringBuilder.toString();
    }

    public String printWorldsSortedByPercentVowelLetters() {
        List<Word> list = new ArrayList<>();

        StringBuilder stringBuilder = new StringBuilder();

        for (Sentence s : sentenceList) {
            for (Word word : s.getWordList()) {
                if (list.indexOf(word) == -1) {
                    list.add(word);
                }
            }
        }

        for (Word w : list) {
            w.setWord(w.getWord().toLowerCase());
        }

        list.sort(Comparator.comparing(this::percentOfVowelLetters));

        for (Word w : list) {
            stringBuilder.append(w).append(" - ").append(Math.round(percentOfVowelLetters(w))).append("\n");
        }
        return stringBuilder.toString();
    }

    public String printWorldsSortedByFirstConsonantLetter() {
        List<Word> list = new ArrayList<>();

        StringBuilder stringBuilder = new StringBuilder();

        Pattern pattern = Pattern.compile("^[AOEUIieauo]+.*");

        for (Sentence s : sentenceList) {
            for (Word word : s.getWordList()) {
                if (pattern.matcher(word.getWord()).find()) {
                    if (list.indexOf(word) == -1) {
                        list.add(word);
                    }
                }
            }
        }

        for (Word w : list) {
            w.setWord(w.getWord().toLowerCase());
        }

        list.sort(Comparator.comparing(this::getFirstConsonantLetter));

        for (Word w : list) {
            stringBuilder.append(w).append("\n");
        }
        return stringBuilder.toString();
    }

    private String getFirstConsonantLetter(Word w) {
        Stream<String> stringStream = Stream.of(w.getWord().split(""));
        Pattern pattern = Pattern.compile("[^AOEUIieauo]");
        Optional<String> letter = stringStream
                .filter(c -> pattern.matcher(c).find())
                .findFirst();
        return letter.toString();
    }

    private double percentOfVowelLetters(Word w) {
        Stream<String> stringStream = Stream.of(w.getWord().split(""));
        Pattern pattern = Pattern.compile("[AOEUIieauo]");
        long value = stringStream
                .filter(c -> pattern.matcher(c).find())
                .count();
        return ((double) value / w.getWord().length()) * 100;
    }

}
