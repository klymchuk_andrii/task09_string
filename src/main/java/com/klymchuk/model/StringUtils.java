package com.klymchuk.model;

import javax.xml.crypto.dsig.spec.XSLTTransformParameterSpec;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class StringUtils {
    public String concatenateAllParameters(Object... objects) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < objects.length; i++) {
            stringBuilder.append(objects[i]);
        }

        return stringBuilder.toString();
    }

    public boolean checkTextForPattern(String text) {
        String regex = "[A-Z].+[.]";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);

        return matcher.find();
    }

    public String splitSentenceByTheAndYou(String text) {
        StringBuilder stringBuilder = new StringBuilder();
        String[] arr = text.split("\\s*(\\sthe\\s|\\syou\\s)\\s*");
        for (String s : arr) {
            stringBuilder.append(s);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public String changeAllVowels(String text) {
        String pattern = "[aouei]";
        return text.replaceAll(pattern, "_");
    }
}
