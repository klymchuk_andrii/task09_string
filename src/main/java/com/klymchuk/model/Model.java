package com.klymchuk.model;

public interface Model {
    String concatenatesAllParameters(Object... objects);

    boolean checkTextForPattern(String text);

    String splitSentenceByTheAndYou(String text);

    String changeAllVowels(String text);

    String getFromFile();

    void printSentences();

    int countOfSentencesWithEqualWords();

    String printSentencesByWordCount();

    String checkUniqueWorld();

    String changeWordsWithFirstVowelsLetter();

    String printAllWords();

    String printWorldsSortedByPercentVowelLetters();

    String printWorldsSortedByFirstConsonantLetter();
}
